class Order 
{
	private String Data;
	//... các loại code khác
	public void processData(ISaveInformation iSaveInformation)
	{
		iSaveInformation.save(Data);
	}
}

interface ISendInformation
{
    public void send();
}

class SendMail implements ISendInformation 
{
    @Override
    public void send() {
        System.out.println("Send Email");
    }
}

interface ISaveInformation 
{
    public void save();
}

class SaveStorage implements ISaveInformation 
{
    @Override
    public void save() {
        System.out.println("Save to Disk");
    }
}

class SendMailAdapter implements ISaveInformation 
{
    ISendInformation sendInformation;

    public SendMailAdapter(ISendInformation sendInformation) 
    {
        this.sendInformation = sendInformation;
    }

    @Override
    public void save(String Data) 
    {
        sendInformation.send(Data);
    }
}
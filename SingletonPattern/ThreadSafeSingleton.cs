using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TheadSafeSingleton
{
    class ThreadSafeSingleton
    {
        private ThreadSafeSingleton()
        {

        }

        private static ThreadSafeSingleton _instanceThreadSafeSingleton;
        private static readonly object _lock = new object();

        public static ThreadSafeSingleton GetInstance(string value)
        {
            if (_instanceThreadSafeSingleton == null)
            {
                lock (_lock)
                {
                    if (_instanceThreadSafeSingleton == null)
                    {
                        _instanceThreadSafeSingleton = new ThreadSafeSingleton();
                        _instanceThreadSafeSingleton.Value = value;
                    }
                }
            }
            return _instanceThreadSafeSingleton;
        }
        public string Value { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                "{0}\n{1}\n\n{2}\n",
                "If you see the same value, then singleton was reused (yay!)",
                "If you see different values, then 2 singletons were created (booo!!)",
                "RESULT:"
            );

            Thread process1 = new Thread(() =>
            {
                TestSingleton("FOO");
            });

            Thread process2 = new Thread(() =>
            {
                TestSingleton("BAR");
            });

            process1.Start();
            process2.Start();

            process1.Join();
            process2.Join();
        }

        public static void TestSingleton(string value)
        {
            ThreadSafeSingleton singleton = ThreadSafeSingleton.GetInstance(value);
            Console.WriteLine(singleton.Value);
            Console.ReadKey();
        }
    }
}

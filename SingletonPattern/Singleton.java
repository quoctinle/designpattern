public class SingleObject {

   //create an object of SingleObject
   private static SingleObject instance = new SingleObject();

   //make the constructor private so that this class cannot be
   //instantiated
   private SingleObject(){}

   //Get the only object available
   public static SingleObject getInstance(){
      return instance;
   }

   public void showMessage(){
      System.out.println("Hello World!");
   }
}

public class SingletonPatternDemo {
   public static void main(String[] args) {

      //illegal construct
      //Compile Time Error: The constructor SingleObject() is not visible
      //SingleObject object = new SingleObject();

      //Get the only object available
      SingleObject object = SingleObject.getInstance();

      //show the message
      object.showMessage();
   }
}

public class LazyInitializedSingleton {

   private static LazyInitializedSingleton instance;

   private LazyInitializedSingleton(){}

   public static LazyInitializedSingleton getInstance(){
       if(instance == null){
           instance = new LazyInitializedSingleton();
       }
       return instance;
       }
}

public class ThreadSafeSingleton {
   private static ThreadSafeSingleton instance;
   private ThreadSafeSingleton(){}

   public static ThreadSafeSingleton getInstance(){
       if(instance == null){
           synchronized(ThreadSafeSingleton.class){
               if(instance == null){
                  instance = new ThreadSafeSingleton();
               }
           }
       }
       return instance;
    }
}
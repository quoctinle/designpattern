using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NaiveSingleton
{
    class NaiveSingleton
    {
        private NaiveSingleton()
        {

        }

        private static NaiveSingleton _instance;

        public static NaiveSingleton GetInstance()
        {
            if (_instance != null)
            {
                _instance = new NaiveSingleton();
            }
            return _instance;
        }

        public static void someBusinessLogic(string _Agurment)
        {
            Console.WriteLine(_Agurment);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            NaiveSingleton s1 = NaiveSingleton.GetInstance();
            NaiveSingleton s2 = NaiveSingleton.GetInstance();

            if (s1 == s2)
            {
                Console.WriteLine("Singleton works, both variables contain the same instance");
            }
            else
            {
                Console.WriteLine("Singleton failed, both variables contain different instance");
            }
            Console.ReadKey();
        }
    }
}

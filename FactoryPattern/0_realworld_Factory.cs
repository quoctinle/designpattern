using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Rectangle = System.Windows.Shapes.Rectangle;
using Brush = System.Windows.Media.Brush;
using Newtonsoft.Json;

namespace DLPredictor
{

    /// <summary>
    /// This is the code where I applied Factory Pattern at work
    /// </summary>
    public abstract class BaseResult
    {
    }

    public class DefectResult : BaseResult
    {
        public Rect DefectRect { get; set; }
        public float Score { get; set; }
        public bool IsNotOverKill { get; set; }
    }

    public class ClassificationResult : BaseResult
    {
        [JsonProperty(PropertyName = "Classes")]
        public List<string> Classes = new List<string>();
        [JsonProperty(PropertyName = "Scores")]
        public List<dynamic> Scores = new List<dynamic>();
        [JsonProperty(PropertyName = "Box")]
        public List<dynamic> Box = new List<dynamic>(); // values meaning: top, left, bottom, right
        [JsonProperty(PropertyName = "FinalResult")]
        public string FinalResult = string.Empty;
    }

    public abstract class VisualizeDefectMethodClassBase
    {
        protected string[] _listdefect;
        protected MainWindow _app;
        protected ModelConfig _curConfig;
        protected int _curImagePredict;

        //public VisualizeDefectMethodClassBase(MainWindow app, int curImagePredict)
        //{
        //    _app = app;
        //    _curImagePredict = curImagePredict;
        //    _curConfig = _app.listModelPredictor[_app.StatsCounter[0].indexModelPredictor[_curImagePredict]].configModel;
        //}

        protected string _defect;
        protected int _toppad;
        protected int _leftpad;
        protected DefectResult _defectResult = new DefectResult();
        /// <summary>
        /// Initialize Paremeter to visualize defects
        /// </summary>
        /// <param name="defectString"></param>
        /// _listdefect[1] = Label Count number
        /// _listdefect[2] = listIntRect = List defects rectangle coordinators
        /// _listdefect[3] = defects score
        /// <param name="toppad"></param>
        /// <param name="leftpad"></param>
        /// <param name="defectResult"></param>
        public void IntializeParameter(string defectString, int toppad, int leftpad, DefectResult defectResult)
        {
            _defect = defectString;
            _toppad = toppad;
            _leftpad = leftpad;
            _listdefect = defectString.Split('_');
            _defectResult = defectResult;
        }

        protected abstract int UpdateLabelCount();

        protected List<int> UpdateListIntRect()
        {
            _listdefect[2] = _listdefect[2].Substring(1, _listdefect[2].Length - 2);
            string[] listStrRect = _listdefect[2].Split(' ');
            List<int> listIntRect = new List<int>();
            foreach (string item in listStrRect)
            {
                try
                {
                    if (item != "" && item != " ")
                    {
                        listIntRect.Add(int.Parse(item));
                    }
                }
                catch
                {

                }
            }
            return listIntRect;
        }
        protected abstract void UpdateDefectRect(Action<Rect> defectRect);

        protected float UpdateScore()
        {
            if (_listdefect.Count() >= 4)
            {
                _defectResult.Score = float.Parse(_listdefect[3]);
            }
            return _defectResult.Score;
        }

        protected abstract Rectangle UpdateOverlayRect(List<string> classnamesList);

        public abstract BaseResult UpdateStatsCounter();

        public abstract void InitializeParams(ClassificationResult inputResult);
    }

    public class VisualizeDefectClassification : VisualizeDefectMethodClassBase
    {
        private ClassificationResult _currentResult;

        public VisualizeDefectClassification(MainWindow app, int curImagePredict)
        {
            _app = app;
            _curImagePredict = curImagePredict;
            _curConfig = _app.ListModelPredictor[_app.StatsCounter[0].indexModelPredictor[_curImagePredict]].configModel;
        }

        /// Deprecated in classification mode
        protected override int UpdateLabelCount()
        {
            int labelCount = int.Parse(_listdefect[1]);
            labelCount += 1;
            return labelCount;
        }

        /// Deprecated
        //protected override Rect UpdateDefectRect(Rect defectRect)
        //{
        //    List<int> listintrect = UpdateListIntRect();
        //    defectRect.X = listintrect[1];
        //    defectRect.Y = listintrect[0];

        //    defectRect.Width = listintrect[3] - listintrect[1];
        //    defectRect.Height = listintrect[2] - listintrect[0];
        //    return defectRect;
        //}

        protected override void UpdateDefectRect(Action<Rect> SetDefectRect)
        {
            Rect defectRect = new Rect();
            defectRect.Width = Math.Abs(_currentResult.Box[3] - _currentResult.Box[1]);
            defectRect.Height = Math.Abs(_currentResult.Box[2] - _currentResult.Box[0]);

            // X - Y are top - left.
            defectRect.X = _currentResult.Box[0];
            defectRect.Y = _currentResult.Box[1];

            SetDefectRect(defectRect);
        }

        public override void InitializeParams(ClassificationResult inputResult)
        {
            _currentResult = inputResult;
        }

        protected override Rectangle UpdateOverlayRect(List<string> classnamesList)
        {
            throw new NotImplementedException();
        }

        protected Rectangle UpdateOverlayRect_v2(string inputClassName, int inputClassID)
        {
            Rectangle overlayRect = null;
            string _rectForeground = string.Empty;
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                overlayRect = new Rectangle();
                var converter = new System.Windows.Media.BrushConverter();
                Rect _rect = new Rect();

                UpdateDefectRect(value => _rect = value);

                BitmapImage image1 = _app.ReadImageFromFile(_app.StatsCounter[0].dictImagePath[_curImagePredict]);
                int imagewidth1 = image1.PixelWidth;
                int imageheight1 = image1.PixelHeight;

                if (_curConfig.LabelForegrounds?.Count - 1 > inputClassID)
                {
                    _rectForeground = _curConfig.LabelForegrounds[inputClassID];
                }
                else
                {
                    _rectForeground = _app.ClassBrushes[inputClassID].ToString();
                }

                overlayRect.Tag = inputClassName;
                overlayRect.Stroke = (Brush)converter.ConvertFromString(_rectForeground);
                overlayRect.StrokeThickness = 1;
                overlayRect.Width = _rect.Width * _app.ImageView.ActualWidth / imagewidth1;
                overlayRect.Height = _rect.Height * _app.ImageView.ActualHeight / imageheight1;
                overlayRect.SetValue(Canvas.LeftProperty, _rect.Left * _app.ImageView.ActualWidth / imagewidth1);
                overlayRect.SetValue(Canvas.TopProperty, _rect.Top * _app.ImageView.ActualHeight / imageheight1);
            });

            return overlayRect ?? overlayRect;
        }

        public override BaseResult UpdateStatsCounter()
        {
            int _curClassID = 0;
            float _curScore = 0;
            string _curClassName = string.Empty;
            List<string> classnames = _curConfig.DictionaryClassName.Keys.ToList();

            for (int i = 0; i < _currentResult?.Classes.Count; i++)
            {
                _curClassName = _currentResult.Classes[i];
                _curClassID = classnames.IndexOf(_curClassName);
                _curScore = (float)Math.Round(_currentResult.Scores[i], 4);

                _app.StatsCounter[_app.curCounterPredict].LabelCount[_curImagePredict].Add(_curClassID);
                _app.StatsCounter[_app.curCounterPredict].Scores[_curImagePredict].Add(_curScore);
                _app.StatsCounter[_app.curCounterPredict].overlayRects[_curImagePredict].Add(UpdateOverlayRect_v2(_curClassName, _curClassID));
            }

            // In this classification mode, there's no need to return because rule is in backend now.
            return _currentResult;
        }
    }
    public class VisualizeDefectOD : VisualizeDefectMethodClassBase
    {
        public VisualizeDefectOD(MainWindow app, int curImagePredict)
        {
            _app = app;
            _curImagePredict = curImagePredict;
            _curConfig = _app.ListModelPredictor[_app.StatsCounter[0].indexModelPredictor[_curImagePredict]].configModel;
        }

        protected override int UpdateLabelCount()
        {
            int labelCount = int.Parse(_listdefect[1]);
            return labelCount;
        }

        protected override void UpdateDefectRect(Action<Rect> SetDefectRect)
        {
            List<int> listintrect = UpdateListIntRect();
            Rect defectRect = new Rect();

            defectRect.X = listintrect[1] - _leftpad +
                           _app.StatsCounter[_app.curCounterPredict].listDefectPosition[_curImagePredict].X -
                           (_curConfig.CropSize) / 2;
            defectRect.Y = listintrect[0] - _toppad +
                           _app.StatsCounter[_app.curCounterPredict].listDefectPosition[_curImagePredict].Y -
                           (_curConfig.CropSize) / 2;

            defectRect.Width = listintrect[3] - listintrect[1];
            defectRect.Height = listintrect[2] - listintrect[0];

            SetDefectRect(defectRect);
        }

        protected override Rectangle UpdateOverlayRect(List<string> classnamesList)
        {
            int labelCount = UpdateLabelCount();
            Rectangle overlayRect;

            UpdateDefectRect(value => _defectResult.DefectRect = value);

            if (classnamesList[labelCount - 1].Contains("Overkill"))
            {
                _defectResult.IsNotOverKill = false;
            }

            BitmapImage image =
                _app.ReadImageFromFile(_app.StatsCounter[_app.curCounterPredict].dictImagePath[_curImagePredict]);
            int imagewidth = image.PixelWidth;
            int imageheight = image.PixelHeight;
            image.Freeze();

            overlayRect = new Rectangle();

            overlayRect.Tag = classnamesList[labelCount - 1];
            overlayRect.Stroke = _app.ClassBrushes[labelCount - 1];
            overlayRect.StrokeThickness = 1;
            overlayRect.Width = _defectResult.DefectRect.Width * _app.ImageView.ActualWidth / imagewidth;
            overlayRect.Height = _defectResult.DefectRect.Height * _app.ImageView.ActualHeight / imageheight;
            overlayRect.SetValue(Canvas.LeftProperty, _defectResult.DefectRect.Left * _app.ImageView.ActualWidth / imagewidth);
            overlayRect.SetValue(Canvas.TopProperty, _defectResult.DefectRect.Top * _app.ImageView.ActualHeight / imageheight);

            return overlayRect;
        }

        /// <summary>
        /// Will comeback to unify this function with 'InitializeParameter'.
        /// </summary>
        /// <param name="inputResult"></param>
        public override void InitializeParams(ClassificationResult inputResult)
        {
            throw new NotImplementedException();
        }

        public override BaseResult UpdateStatsCounter()
        {
            List<string> classnames = _curConfig.DictionaryClassName.Keys.ToList();

            _app.StatsCounter[_app.curCounterPredict].LabelCount[_curImagePredict].Add(UpdateLabelCount());
            _app.StatsCounter[_app.curCounterPredict].Scores[_curImagePredict].Add(UpdateScore());
            _app.StatsCounter[_app.curCounterPredict].overlayRects[_curImagePredict].Add(UpdateOverlayRect(classnames));

            return _defectResult;
        }
    }

    public class VisualizeDefectFactory
    {
        public static VisualizeDefectMethodClassBase CreateInstanceCompatibleWithMode(MainWindow _app, int curImagePredict, SwitchModeWindow.PredictionMode selectedOption)
        {
            VisualizeDefectMethodClassBase currentMethod;
            switch (selectedOption)
            {
                case SwitchModeWindow.PredictionMode.Detection:
                    currentMethod = new VisualizeDefectOD(_app, curImagePredict);
                    break;
                case SwitchModeWindow.PredictionMode.Classification:
                    currentMethod = new VisualizeDefectClassification(_app, curImagePredict);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return currentMethod;
        }
    }
}
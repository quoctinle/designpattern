using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Controls;

namespace ButtonLib
{
    public class ButtonControlFactory
    {
        public static ButtonControlBase CreateButtonControl(ButtonControlMode buttonControlMode)
        {
            ButtonControlBase buttonControl = null;

            switch (buttonControlMode)
            {
                case ButtonControlMode.FourButtons:
                    buttonControl = new FourButtonsControl();
                    break;

                case ButtonControlMode.SixButtons:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            return buttonControl;
        }
    }
    public enum ButtonMode
    {
        Next,
        Previous,
        GoToFirst,
        GoToLast,
        JumpUp,
        JumpDown
    }

    public enum ButtonControlMode
    {
        FourButtons,
        SixButtons
    }

    public class ButtonType
    {
        public string buttonName;
        public ButtonMode buttonMode;
        public Button button;

        public ButtonType(Button senderButton, ButtonMode mode)
        {
            button = senderButton;
            buttonMode = mode;
            buttonName = GetButtonName();
        }

        private string GetButtonName()
        {
            return button.Name;
        }
    }

    public abstract class ButtonControlBase
    {
        public bool isRealButtons;
        public int currentListCount;
        public int currentIndex;
        public ButtonMode currentButtonMode;
        public List<ButtonType> listButtons;

        public abstract void Initialize(bool isReal, List<ButtonType> listOfButtons = null);

        public abstract void Button_OnClickHandler(Button button, ref int currentIndex, int listCount);

        public abstract void VirtualButton_OnClickHandler(ButtonMode mode, ref int currentIndex, int listCount);

        protected abstract int UpdateIndexValue_BaseOnButtonMode(int inputIndex);

        protected abstract void UpdateButtonsStatus();

        protected abstract bool ValidateListButtonsCount();

        protected int CheckIndexNegative_Or_IndexOutOfRange(int inputIndex)
        {
            // Index = 0 and ListCount <= 0 will be affect by following condition, so we need Index != 0 & ListCount > 0
            if (inputIndex != 0 && currentListCount > 0)
            {
                if (inputIndex < 0)
                {
                    inputIndex = 0;
                    Debug.WriteLine(string.Format("Index is Negative. Index: {0} List Count: {1}", inputIndex, currentListCount));
                }
                else if (inputIndex > currentListCount - 1)
                {
                    inputIndex = currentListCount - 1;
                    Debug.WriteLine(string.Format("Index is Out of range. Index: {0} List Count: {1}", inputIndex, currentListCount));
                }
            }
            return inputIndex;
        }

        protected ButtonMode GetButtonMode(string senderButtonName)
        {
            foreach (var item in listButtons.Where(btn => btn.buttonName == senderButtonName))
            {
                return item.buttonMode;
            }

            throw new ArgumentOutOfRangeException();
        }

    }
    public class FourButtonsControl : ButtonControlBase
    {
        public override void Initialize(bool isReal, List<ButtonType> listOfButtons = null)
        {
            isRealButtons = isReal;
            listButtons = listOfButtons != null ? listOfButtons : null;

            if (listButtons != null && !ValidateListButtonsCount())
            {
                throw new ArgumentOutOfRangeException("The list quantities must be 4.");
            }
        }

        protected override bool ValidateListButtonsCount()
        {
            if (listButtons.Count == 4) // We are controlling 4 buttons.
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The method helps to update index and real button status.
        /// </summary>
        /// <param name="inputButton"></param>
        /// <param name="inputIndex"></param>
        /// <param name="listCount"></param>
        public override void Button_OnClickHandler(Button inputButton, ref int inputIndex, int listCount)
        {
            if (isRealButtons)
            {
                currentButtonMode = GetButtonMode(inputButton.Name);
                currentListCount = listCount;

                inputIndex = UpdateIndexValue_BaseOnButtonMode(inputIndex);
                currentIndex = inputIndex;

                Debug.WriteLine(string.Format("Current index value: {0}", currentIndex));

                UpdateButtonsStatus();
            }
            else
            {
                throw new ArgumentException("Using {VirtualButton_OnClickHandler()} function instead!");
            }
        }

        /// <summary>
        ///  This method just helps to update index.
        ///  Use this method for virtual buttons.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="inputIndex"></param>
        /// <param name="listCount"></param>
        public override void VirtualButton_OnClickHandler(ButtonMode mode, ref int inputIndex, int listCount)
        {
            if (!isRealButtons)
            {
                currentButtonMode = mode;
                currentListCount = listCount;

                inputIndex = UpdateIndexValue_BaseOnButtonMode(inputIndex);
                currentIndex = inputIndex;
                Debug.WriteLine(string.Format("Current index value: {0}", currentIndex));
            }
            else
            {
                throw new ArgumentException("Using {Button_OnClickHandler()} function instead!");
            }
        }

        protected override int UpdateIndexValue_BaseOnButtonMode(int inputIndex)
        {
            switch (currentButtonMode)
            {
                case ButtonMode.GoToFirst:
                    return 0;

                case ButtonMode.Previous:
                    return CheckIndexNegative_Or_IndexOutOfRange(--inputIndex);

                case ButtonMode.Next:
                    return CheckIndexNegative_Or_IndexOutOfRange(++inputIndex);

                case ButtonMode.GoToLast:
                    return currentListCount - 1;

                default:
                    return inputIndex;
            }
        }

        protected override void UpdateButtonsStatus()
        {
            if (currentListCount <= 1)
            {
                IsEnableAllButtons(false);
            }
            else if (currentIndex > 0 && currentIndex < currentListCount - 1)
            {
                IsEnableAllButtons(true);
            }
            else if (currentIndex <= 0)
            {
                IsEnableFirstAndPreviousButton(false);
                IsEnableLastAndNextButton(true);
            }
            else if (currentIndex >= currentListCount - 1)
            {
                IsEnableFirstAndPreviousButton(true);
                IsEnableLastAndNextButton(false);
            }
        }

        void IsEnableLastAndNextButton(bool isEnabled)
        {
            foreach (var item in listButtons.Where(btn => btn.buttonMode == ButtonMode.GoToLast || btn.buttonMode == ButtonMode.Next))
            {
                item.button.IsEnabled = isEnabled;
            }
        }

        void IsEnableFirstAndPreviousButton(bool isEnabled)
        {
            foreach (var item in listButtons.Where(btn => btn.buttonMode == ButtonMode.GoToFirst || btn.buttonMode == ButtonMode.Previous))
            {
                item.button.IsEnabled = isEnabled;
            }
        }

        void IsEnableAllButtons(bool isEnabled)
        {
            listButtons.All(item => item.button.IsEnabled = isEnabled);
        }

    }
}